import React from "react";
import Link from "gatsby-link";

import Button from "../components/Button.jsx";

const FeaturedPost = (props) => {
  return (
    <div className="featured-post"
      style={{ backgroundImage:`url(${props.postData.node.frontmatter.image})` }}
    >
      <div className="featured-post-content">
        <h1 className="featured-post-title"> {props.postData.node.frontmatter.title} </h1>
        <p className="featured-post-excerpt"> {props.postData.node.excerpt} </p>
        <div className="featured-btns">
          <Button
            postLink = {props.postData.node.fields.slug}
          />
        </div>
      </div>
    </div>
  )
}

export default FeaturedPost;