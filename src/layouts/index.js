import React from 'react';
import PropTypes from 'prop-types';
import Link from 'gatsby-link';
import Helmet from 'react-helmet';

import Logo from '../components/Logo.jsx';

require("prismjs/themes/prism-tomorrow.css");
import './index.scss';

const Header = () => (
  <h1 style={{ margin: 0 }}>
    <Link
      to="/"
    >
      <Logo/>
    </Link>
  </h1>
);

const TemplateWrapper = ({ children, data }) => (
  <div>
    <Header title={data.site.siteMetadata.title} />
    {children()}
  </div>
);

TemplateWrapper.propTypes = {
  children: PropTypes.func,
};

export default TemplateWrapper;

export const query = graphql`
  query TemplateWrapperQuery {
    site {
      siteMetadata {
        title
      }
    }
  }`;
