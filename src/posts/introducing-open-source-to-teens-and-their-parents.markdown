---
title:  "Introducing Open Source to teens and their parents."
date:  "2018-10-16"
categories: Open Source
author: Inder Singh
image: "/publicImages/gci.JPG"
comments: true
keywords: "GCI, GSoC, Los Angeles, Public Speaking, Teens, Kids, Codewizards"
---
**“Why would these programmers work for free?”** asked one of the students as I stood in a room full of curious teenagers who had never heard of open source before. 

These students had gathered on a Sunday morning inside a Sikh school in Los Angeles to know about Google Code-in, Open Source, and programming in general. Trying to answer the question, I started going through my personal journey in Open Source. I told them how my contribution to BRL-CAD got me into Google Summer of Code. New technologies that I learnt in GSoC helped me grab my first job. Being a mentor at GCI for BRL-CAD was one of the key aspects that eventually got me hired as a curriculum developer at Codewizards (a coding school for middle schoolers). In a nutshell, I owe my career to open source. I have gained and learnt so much from the open source community that it becomes obvious to return the favour. For me and many others alike,**working for open source is not free, it's priceless**.


## **“Do people even use open source software?”**
“Do people even use open source software?” asked another student and I started reading the statistics from a piece of paper that I had in my pocket. I started listing the companies and organizations that depend on open source. Soon they started exchanging looks in disbelief, probably asking themselves "Why haven't I heard of open source before?" and that's the question I have been asking myself a lot ever since "Why are so many kids, even those who have interest in coding, unaware of open source?"

Children by nature are curious and creative. Having spent so much time on tablets, mobiles and other gadgets, they want to know what goes behind creating all this technology. So, whenever I introduce them with the open source process of choosing an existing software, editing a part of it and making it their own; these young minds seem to fall in love immediately with this concept. 

## **“How do I start?”**
After having incited interest in open source, I jumped to the most common question asked “How do I start?” and, most of the time, my answer is Google Code-in (GCI), a contest organized around open source contribution for students of 13 to 17 years of age. With this, the students got excited about the opportunity to visit Google headquarters and many of them decided to participate in Google Code-In.

Daily schedule of this age group is tangled up in lots of activities. Parents and teachers play a huge role in deciding where the students spend their time, so reaching out to them as well is important. Once they know about open source, teachers and parents are very encouraging. For instance, principal of the school where I had this session, appointed one of the senior students to help others in getting started with GCI. She wanted to make sure that students get all the aid they need.

## **“Will my kid be able to do it?”**
A week after my talk in Los Angeles, I flew to Austin (Texas) where students from different schools accompanied their parents to a parent-info session organized by Codewizards at Cedar Valley Middle School. The objective was to make parents aware of Open Source, GCI, and how it could help their kids. The session was highly interactive where we spent more than 50% of the time in QnA. A lot of questions were asked and answered but one of the major themes was “My girl or my boy has never done programming before, will she/he be able to do these tasks?”. I explained that GCI has various kinds of tasks and programming is just one among the 5 different categories. Still, "contributing to open source software" sounded technical and non-approachable. There seemed to be a doubt "Will he/she be able to do it?" Hence, I urged them to think of it as swimming: to get started you need to jump into the water first. What GCI provides is a safety net and a community of world-class swimmers (programmers), keen to guide and help the newbies make their first lap around the pool.

While I was explaining about GCI and Open Source, we had an interesting conversation, when parents, to none of their fault, mistook the number 4553 to be the students participating "per year". I had to explain that 4553 was the total number of students that had participated in the entire history of Google Code-in (until 2017). Though this number is big for us, the open source developers, it clearly indicated that we can and must improve. As expected, I got the same reaction from these parents as that from the students in LA - **why haven't so many of us heard of GCI and Open Source before?**

## **“why haven't so many of us heard of GCI and Open Source before?”**
That’s rather a good question as it indicates that they wish they had known about it earlier and they have understood the impact open source can have. If “It (Programming) teaches you how to think” (a quote by Steve jobs) then Open source programming teaches you how to think open-mindedly. It teaches virtues of collaboration, sharing, inclusivity, generosity, and working in a community to name a few. I am yet to meet a parent who is not big on passing these virtues to their kids. These virtues and skills are useful even if they do not end up becoming professional programmers or computer scientists. Mitch Resnick in his ted talk makes a beautiful analogy when he says,

>"When you become fluent with reading and writing, it's not something that you're doing just to become a professional writer. Very few people become professional writers. But it's useful for everybody to learn how to read and write. Again, the same thing is with coding." 

2017 was the year when **GCI broke records**, a total of 3529 students participated, out of which 50 students will win the grand prize. These thousands of students might not end up becoming professional programmers or computer scientists, but they were still able to contribute to open source. Furthermore, 2017 was the year, when, for the first time ever, I talked about open source to non-programmers, something that I would  do more often, something that all of us should do more often. GCI is one of the gateways through which thousands of budding contributors are making an entrance to the open source world. Let us be warm, welcoming, prepared, and as helpful as we can be.

