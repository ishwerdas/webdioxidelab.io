---
title:  "The science behind a beautiful design."
date:  "2018-11-04"
categories: design theory, design, web design, beautiful design
author: Panda
image: "/publicImages/symmetry.jpeg"
---

Like many people, I think of myself as a design enthusiast. I have experienced that wow moment when I see a beautiful design. I get this nice comfortable feeling when I see a beautiful web page.

> Why's that? Let's figure out.

## Brain on symmetry
Our brains are accustomed to symmetry since our ancestors. The very first tools created by us were symmetrical in shape. The purpose of crafting them was not just to get the work done, but to make them beautiful as well.

In nature, everything we see is following the symmetry. Stems, trees, flowers all grow in a symmetrical shape. Thus, we started to evaluate the environment for patterns and based on that react quickly to the danger (asymmetry). It hardwired this in our brain that symmetry means safe and asymmetry means something is wrong.

And, in today's world, we have developed enough that we don't need to survive on a daily basis. But our brain still activates the nice warm feeling when we see the symmetrical design. And also that irksome feeling when we see an asymmetrical design.

> If you have 7 minutes and 34 seconds, I would recommend watching this video.
> <iframe width="560" height="315" src="https://www.youtube.com/embed/-O5kNPlUV7w?controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Laws of Design
There are certain design laws that not just make our interface beautiful but efficent to use as well.
> Design is not just what it looks like and feels like. Design is how it works. 
> 
> _Steve Jobs_

In this post, I will discuss just the one law which is commonly used in interface design, in order to increase the user turnout.

### Fitts Law
This law demonstrates how we can reverse engineer our behaviour in order to get a efficent design. 

In 1954, a psychologist named Paul Fitts, studied the human movements. He concluded that the time required to move to a target
- depends on the distance to it
- And inversely to it's size

Thus penned down the following statement:
> The time to acquire a target is a function of the distance to and size of the target.

As per this law, if the signup button in our page is bigger and closer to reach, more users would signup as compared to the one that is hidden in the pile of other menus. Now this sounds pretty basic, but when we are geeky enough to use mathematical formulas to calculate the size and position of a element on page, then there is when magic happens.

### Fitts Law in real life
This law isn't just limited to web pages and software interfaces. It can be applied anywhere.

You might have already noticed this. Next time you get inside a car, take a look at foot pedals. You will find that the brake pedal is bigger and closer to the seat as compared to the accelerator pedal. And this is universally true in all the vehicles.

Fitts law is subtle but once we notice, it all makes more sense, right?

If the brake pedal is bigger and closer, it's quicker and easier to stop than to accelerate.

## Other UI/UX laws
Make sure to check out this awesome site [lawsofux.com](https://lawsofux.com) to learn about different design laws.