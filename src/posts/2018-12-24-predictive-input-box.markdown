---
title:  "Making simple things intuitive: Predictive input"
date:  "2018-12-24"
categories: input, datalist, html5, interactive, input box, form, list attribute
author: Panda
image: "/publicImages/alphabet.jpeg"
---

I am sorry to break it to you, but we are in a constant state of war. A war to make things more user interactive. The only enemy here is the non-friendly UI. It's the best kind of war we can get.

Want to skip the blah, [click here](#predictive-input-the-tutorial) to go straight to the tutorial.

> If you want an example of war, take a look at the pace of notched smartphones. Did we even know the word "notched display" a year ago? No. But within a few months, every smartphone manufacturer has adopted it. They are in a race, to fix what Apple did wrong with iPhone X. Well, that's an argument for a separate post.

Keeping the above war in mind, I'll be posting a series of posts (this being the first in that series), which encourage making simple things intuitive without much effort.

## Input boxes & select list
We have been using input boxes since ages. And now there are many things we can do with it. We can validate an input box for a date, email, a phone number without writing those complex regular expressions ourselves. But if you are still using the same old technique of regular expressions to validate input, this is how you might look.

![Stone age](/publicImages/stoneage.gif)

The Input box is ideal for values which are different for everyone like, your name, your phone number. But for items, where a user has to choose from a list of values, we use something called select list. In that, we select a single value from the given dropdown list.

Select saves the time of typing entire value. But we can only choose from the given values. What if, we need to enter something that's not on the list?

Or even worse, imagine the list is so long, that it takes you hours to find what you need.

![Loooooong list](/publicImages/longlist.gif)

## Predictive input: the tutorial
For those who read my above blah, you are my kind of people, much love.❤ For those who skipped to this part directly, I appreciate the honesty and cut to chase approach, but you don't get much love ha! 

### Step 1: Create a simple input type text tag
Let's start with a basic input type text. I'd hope we all know what the following does. It will create a simple input in which we can type stuff.

```html
<input type='text' placeholder='You sir, can type here'>
```

### Step 2: Add the magical datalist tag
Now, for that predictive magic, we all wanted. Let's add a tag named 'datalist'. Give it some id and add its closing tag.
```html{2-3}
<input type='text' placeholder='You sir, can type here'>
<datalist id='secret-agents'>
</datalist>
```

### Step 3: Add options in the datalist tag
Just like we add options in our select tag. We can add the same in the datalist tag. I am assuming you are familiar with the select tag. _Don't let me down._
```html{3-6}
<input type='text' placeholder='You sir, can type here'>
<datalist id='secret-agents'>
   <option>Johny English</option>
   <option>James Bond</option>
   <option>Perry, the platypus</option>
   <option>Agent K</option>
</datalist>
```

### Last Step: Add the list to input
Now we have the list and input ready. Use the list attribute of input tag to link datalist to our input.

```html{1}
<input type='text' placeholder='You sir, can type here' list='secret-agents'>
<datalist id='secret-agents'>
   <option>Johny English</option>
   <option>James Bond</option>
   <option>Perry, the platypus</option>
   <option>Agent K</option>
</datalist>
```

### Here's the miracle

![Output](/publicImages/input.gif)


## Browser compatibility
Few mobile browsers and Safari desktop browser don't support it yet. But Safari technology preview release supports it. So there's a hope that upcoming stable releases will support it too.

[![compatibility table](https://res.cloudinary.com/ireaderinokun/image/upload/v1545658276/caniuse-embed/datalist-2018-12-24.png)](http://caniuse.com/#feat=datalist)

![That's all folks](/publicImages/theend.gif)
