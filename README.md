# gatsby-starter-default
The default Gatsby starter

For an overview of the project structure please refer to the [Gatsby documentation - Building with Components](https://www.gatsbyjs.org/docs/building-with-components/)

Install this starter (assuming Gatsby is installed) by running from your CLI:
```
gatsby new gatsby-example-site
```

a change to trigger

## Deploy

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/gatsbyjs/gatsby-starter-default)

## To add a new post 
1. Go to src/posts/ folder, add your post with today's date and title of posts as name of the file. The extension of the file will be .markdown. Look other post files name for reference.
2. Write the post in markdown.
3. Save it.
4. Commit and push in your branch
5. Send the PR

## Post Guidelines
1) Choose correct date
2) Do not add more than one post per day. Use draft to save posts for thursday etc (when no on publishes a post) 
3) Please add a relevant image, keywords etc when you add a post
4) After publishing look at the post, you might want to change the length of first paragraph, or change titles / headings etc for a better formatting. 
5)  Try not to add more than 3 or 4 lines in first paragraph. First paragraph is increased in size and is used as transition to content.
6) when adding code snippets you can write language name '''javascript or '''bash
7) start headings from h2
